<?php

namespace Helium\FlexModel;

use Illuminate\Database\Eloquent\Model;

/**
 * @warning To query any of the model's "flexible" data fields,
 * 			you should use the following format: FlexModel::where('data->field', 'value');
 * 			where field is the name of the field you are querying
 */
abstract class FlexModel extends Model
{
	protected $jsonAttribute = 'data';

	protected $baseAttributes = [];


	public function unpackJson()
	{
		$jsonField = $this->{$this->jsonAttribute};

		if ($jsonField) {
			$data = is_array($jsonField) ? $jsonField : json_decode($jsonField,	true);

			foreach ($data as $key => $value) {
				$this->$key = $value;
			}

			unset($this->{$this->jsonAttribute});
		}
	}

	public function packJson()
	{
		$jsonAttributes = collect($this->getAttributes())->except($this->baseAttributes);

		foreach ($jsonAttributes as $key => $value) {
			unset($this->$key);
		}

		$this->{$this->jsonAttribute} = json_encode($jsonAttributes->toArray());
	}

	public function save(array $options = [])
	{
		try {
			$result = parent::save($options);

			if (!$result) {
				$this->unpackJson();
			}

			return $result;
		} catch (\Throwable $t)	{
			$this->unpackJson();

			throw $t;
		}
	}

	protected static function boot()
	{
		parent::boot();

		static::saving(function (FlexModel $m) {
			$m->packJson();
		});

		static::saved(function (FlexModel $m) {
			$m->unpackJson();
		});

		static::retrieved(function (FlexModel $m) {
			$m->unpackJson();
		});
	}
}